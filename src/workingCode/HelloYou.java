/*  
 *  CAB302 Lecture 1 demo program - Hello You
 * 
 *  This program extends Hello World with some keyboard input.
 *  You can copy the declarations of "keyboard" and "screen"
 *  below into programs that do console input and output
 *  
 *  In the lecture we'll develop this program from scratch
 *  using Eclipse
 *  
 */

package workingCode;

import java.io.*;
import java.util.Scanner;

public class HelloYou {

	/* A simple program to print to and read from the console
	 */
	public static void main(String[] args) throws IOException {
		
		/* Declare a "screen" object as an instance of the
		 * java.io.PrintWriter class with character stream
		 * java.lang.System.out as its Writer parameter
		 */
		PrintWriter screen = new PrintWriter(System.out, true);
		
		/* Declare a "keyboard" object as an instance of the
		 * Scanner class with byte stream
		 * java.lang.System.in as its parameter
		 */
		Scanner keyboard = new Scanner(System.in);
		
		/* Declare a string object to hold the user's name */
		String name;
		
		/* Ask for the user's name, read it, then echo it */
 		screen.print("Hello. What's your name? ");
 		screen.flush(); // Print output buffer without a newline
  		name = keyboard.nextLine();
  		screen.println("Pleased to meet you, " + name + ".");	
	}

}
