/*  
 *  CAB302 Lecture 1 demo program - Happy Birthday
 * 
 *  A small Java program that contains all the basic control
 *  constructs: assignment, sequential composition, choice,
 *  iteration and a subroutine
 *  
 *  In the lecture we'll start with a buggy version of this
 *  program, containing both syntax and semantic errors, and
 *  fix them with the aid of Eclipse's editing and debugging
 *  features, respectively
 *  
 */

package workingCode;

import java.io.*;
import java.util.Scanner;
import java.util.InputMismatchException;

public class HappyBirthday {

	/* Given a positive integer and a word, return a text string
	 * containing the specified number of space-separated copies of
	 * the word
	 */ 
	private static String copyWord(String word, int howMany) {
		String text = "";
		for (int year = 1; year < howMany; year++) {
				text = text + word + " ";
		};
		text = text + word; // Last word doesn't have a space after it
		return text;
	}
	
	/* Ask the user how old they are, and wish them a
	 * proportionately-sized happy birthday
	 */
	public static void main(String[] args) {
		
		/* Declare standard console i/o streams */
		PrintWriter screen = new PrintWriter(System.out, true);
		Scanner keyboard = new Scanner(System.in);
		
		/* Declare an integer to hold the user's age */
		int age;
		
		/* Ask for the user's age */
 		screen.print("Hello. How old are you? ");
 		screen.flush(); // Print output buffer without a newline
  		
 		/* Attempt to read a number, otherwise use a 
 		 * nonsensical value
 		 */
 		try {
  			age = keyboard.nextInt();
  		} catch (InputMismatchException notAnInt) {
  			age = -1;
  		};
  		
  		/* Wish them a happy birthday if their age was a
  		 * believable number
  		 */
 		if (age <= 0 || age >= 120) {
 			screen.println("That's not your age!");
 		} else {
 			screen.println("Well, have a " + 
 					       copyWord("happy", age) +
 					       " birthday!");
 		}
 		}
	}

