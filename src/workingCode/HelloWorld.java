/*  
 *  CAB302 Lecture 1 demo program - Hello World
 * 
 *  This is the traditional Hello World program in Java
 *  
 *  In the lecture we'll develop this program at the command
 *  line and in Eclipse, to see the difference an IDE makes
 *  
 */

package workingCode;

/* Classic Hello World program
 */
public class HelloWorld {

	public static void main(String[] args) {
		System.out.println("Hello, World!"); // Simple form of console output

	}

}
